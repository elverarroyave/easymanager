package com.easymanager.easymanager.product.io.web.v1.model;

import com.easymanager.easymanager.product.service.model.ProductSaveCmd;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.UniqueElements;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
public class ProductSaveRequest {

    @NotNull(message = "Es obligatorio ")
    @NotBlank(message = "No puede estar vacio ")
    private String name;

    @NotNull(message = "Es obligatorio ")
    @NotBlank(message = "No puede estar vacio ")
    private String code;

    private String brand;

    @Size(min = 3,max = 250, message = "La descripción debe ser entre 3 y 250 caracteres")
    private String description;

    private Long category;

    public static ProductSaveCmd toModel(ProductSaveRequest productToCreate){
        return ProductSaveCmd.builder()
                .name(productToCreate.getName())
                .code(productToCreate.getCode())
                .brand(productToCreate.getBrand())
                .description(productToCreate.getDescription())
                .category(productToCreate.getCategory())
                .build();
    }
}
