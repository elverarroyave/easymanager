package com.easymanager.easymanager.product.model;

import com.easymanager.easymanager.category.model.Category;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.time.LocalDateTime;

import javax.persistence.*;

@Data
@Builder(toBuilder = true)
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "PRODUCT")
@Table(name = "PRODUCT")
public class Product extends RepresentationModel<Product> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String code;

    private String brand;

    private String description;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CATEGORY_ID", referencedColumnName = "id")
    private Category category;

    private LocalDateTime createDate;

    private LocalDateTime updateDate;

}
