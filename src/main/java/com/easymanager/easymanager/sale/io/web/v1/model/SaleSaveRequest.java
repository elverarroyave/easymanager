package com.easymanager.easymanager.sale.io.web.v1.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class SaleSaveRequest {

    private String clientNumDocument;

    private boolean isCredit;

    private int paymentAmount;

    private Long paymentMethod;

}
