package com.easymanager.easymanager.order.service.order;

import com.easymanager.easymanager.inventory.model.Inventory;
import com.easymanager.easymanager.inventory.service.InventoryGateway;
import com.easymanager.easymanager.supplier.model.Supplier;
import com.easymanager.easymanager.supplier.service.SupplierGateway;
import com.easymanager.easymanager.order.model.Orden;
import com.easymanager.easymanager.order.model.OrderDetail;
import com.easymanager.easymanager.order.service.order.model.ItemOrder;
import com.easymanager.easymanager.order.service.orderDetail.OrderDetailsGateway;
import com.easymanager.easymanager.product.model.Product;
import com.easymanager.easymanager.product.service.ProductGateway;
import com.easymanager.easymanager.role.service.RoleGateway;
import com.easymanager.easymanager.user.model.User;
import com.easymanager.easymanager.user.service.UserGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderGateway orderGateway;

    @Autowired
    private OrderDetailsGateway orderDetailsGateway;

    @Autowired
    private ProductGateway productGateway;

    @Autowired
    private SupplierGateway supplierGateway;

    @Autowired
    private UserGateway userGateway;

    @Autowired
    private RoleGateway roleGateway;

    @Autowired
    private InventoryGateway inventoryGateway;

    @Override
    public Orden create(@NotNull String nit, @NotNull Long idPaymentMethod, @NotNull List<ItemOrder> itemOrders) {

        // Search supplier to will order
        Supplier supplierToOrder = supplierGateway.findByNit(nit);

        //User to receive order
        User userToOrder = userGateway.findById(999L);

        // Verify
        if(!userToOrder.getRolesOfUser().contains(roleGateway.findById(4L))){
            throw new RuntimeException("No estas autorizado para recepcionar esta orden");
        }

        // We create a list products details
        List<OrderDetail> productsDetails = new ArrayList<>();

        //Add items to the list product
        for(ItemOrder itemOrder : itemOrders){
            Product productInDataBase = productGateway.findByCode(itemOrder.getCode());
            OrderDetail productDetail = new OrderDetail(
                    itemOrder.getQuantity(),
                    productInDataBase.getCode(),
                    productInDataBase.getName(),
                    itemOrder.getNewPrice()
            );
            productsDetails.add(productDetail);
            //update stock in inventory
            Inventory inventory = inventoryGateway.findByProductId(productInDataBase.getId());
            inventory.updateStock(itemOrder.getQuantity());
        }

        //Create order
        Orden orderToCreate = new Orden();

        // Add attributes to order
        orderToCreate.setSupplier(supplierToOrder);
        orderToCreate.setProductsDetails(productsDetails);
        orderToCreate.setState(1);//Añadimos el estado inicial de la orden.
        orderToCreate.setIdPaymentMethod(idPaymentMethod);
        //TODO: Añadir la funcionalidad para crear la factura de la orden

        // Add order to user
        orderToCreate.setUser(userToOrder);

        //Persist order
        Orden orderCreated = orderGateway.save(orderToCreate);

        // Persist product details
        productsDetails.forEach(productDetail->productDetail.setOrden(orderCreated));
        orderDetailsGateway.saveAll(productsDetails);

        return orderCreated;
    }
}
